---
title: Announcing The Thought Trap (book preorder)
layout: post
author: Nate Levesque
tags: [upstream-neutrality]
head-image: the-thought-trap.png
---

We have nearly unlimited information, social connections, and entertainment at our fingertips almost everywhere via the Internet. The scale of the services that bring that web to us are mind boggling. Over [68% of adults in the U.S. are on Facebook](https://www.pewinternet.org/2018/03/01/social-media-use-in-2018/). Google alone saw [at least 5.5 billion daily searches](https://searchengineland.com/google-now-handles-2-999-trillion-searches-per-year-250247) in 2016 and holds over two-thirds of the United States search market. [Amazon Web Services hosted over 148,000 websites](http://social.techcrunch.com/2017/02/28/amazon-aws-s3-outage-is-breaking-things-for-a-lot-of-websites-and-apps/) as of 2017.

But, how often do we think about those services' stewardship of the Internet? They curate the content we see, make decisions about who gets to speak on their platforms, and sometimes take down whole websites. And, they're within their rights to do so -- they have the same rights to free speech that we do. While they tell us that our content is important to us and that they take their content curation seriously, we don't know what goes on behind the scenes. Nonetheless, we expect them to uphold a standard of fairness and neutrality, while protecting us from the worst the Internet has to offer.

Things aren't actually so utopian on today's web. We’re overwhelmed with information. Our digital lives are tracked and analyzed. Our social feeds show us clickbait, hoaxes, and habit-forming features. New technologies have given us fake photos, Deepfake video, fabricated audio, and even realistic but completely computer-generated speeches from world leaders. They make for propaganda like no other -- we can barely trust our eyes and ears when it comes to the contents of our feeds. Even the tools our social networks give us to report those problems are getting used against us. Bias creeps into our personalized recommendations, our social feeds, and even our search results. We've seen our cloud giants -- Facebook, Google, Twitter, and others -- blamed for bias in their moderation and site features on more than one occasion.

And, despite our best efforts, we're not immune. The web is built, intentionally or not, to exploit how our minds work to turn a profit. Mob mentality, confirmation bias, and [our willingness to re-share without double checking](https://www.washingtonpost.com/news/the-intersect/wp/2016/06/16/six-in-10-of-you-will-share-this-link-without-reading-it-according-to-a-new-and-depressing-study/) make it easy for us to be manipulated -- to trade the commodity that is our attention, to change our opinion, or to sell us something. We don't have many tools to know who is manipulating us, why they're doing it, or when it's happening.

_The Thought Trap_ examines the dystopian side to our web of unlimited information. How are we getting manipulated? To what end? What’s happening across the threshold of the massive online platforms we’re all part of?

Now available for preorder on e-book, launching (alongside a paperback edition) August 7th, 2019. Get more details at https://book.natelevesque.com/books/the-thought-trap.
