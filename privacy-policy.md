---
title: Privacy Policy
layout: strata
---

# Privacy Policy

This website collects information about your visit and what you do while during your visit. This policy describes how information collected from you or your device, or information that you provide, will be handled.

Last Updated 2022/02/09.

## Cookies

Third-party services used by this site may store something called "cookies" on your device. "Cookies" are small pieces of information sent to your browser and stored on your device, and reported back to the website when you load pages.

Data stored in cookies can include anonymised data about your visit such as how you use the site, the URL of the website you came from if you clicked on a link to get to this website, your IP address, and how often and for how long you use the site. Cookies expire after varying periods of time.

This site does not use cookies.

#### Opt Out:
This site does not use cookies. If it did, using your browser's options, you are free to delete cookies or block them entirely at any time.


## Email Subscriptions

Like many blogs, you are free to provide your email address in order to receive notifications of new blog posts or other news. This feature is provided by a third-party service, ConvertKit.

If you request an email subscription, your email address, subscription date, and subscription status (whether you've confirmed your subscription and whether your email address is still active) is retained and stored in the ConvertKit service. This data is never shared or sold by this website, but may also be covered by a privacy policy at ConvertKit.

If you have an active (confirmed) subscription, ConvertKit may collect information about what links you click inside the emails you receive from ConvertKit and whether you've opened an email. You can find the ConvertKit privacy policy at https://convertkit.com/privacy. This data is never shared or sold by this website, but may also be covered by the ConvertKit privacy policy.

If this site migrates to a different subscription service, your email address may be downloaded and imported into the new service, but will not be retained unencrypted anywhere outside the service(s) longer than necessary. This may be done without notice, but changes will be noted in this privacy policy.

#### Opt Out:
* If you have not subscribed: Don't provide your email address to be added to the mailing list. You may also change your mind and ignore the confirmation email (you will receive no further emails, but your email address will be retained)
* If you have subscribed: Unsubscribe at any time with the instructions at the bottom of any email from this site or ConvertKit. You can avoid being tracked in emails by setting your email client to not display images by default and by not clicking links in the emails you receive.

Note about unsubscribing: this site previously provided email subscriptions through Google Feedburner. If you have emails from this site sent to you prior to February 9, 2022 ("the migration date"), the unsubscribe links will no longer unsubscribe you from this list. You will need to use an email sent after the migration date to unsubscribe.

### RSS/ATOM Feed Use

RSS/ATOM feeds are provided through Google Feedburner.

Google Feedburner also tracks various metrics about use of the RSS/ATOM feed it re-publishes from this site including types of feed readers used, items clicked on, number of subscribers, and various other information. This data is generally anonymous, but could reveal your identity if you use an obscure feed reader. Regardless, this data is not shared by this website but may be also be governed by a privacy policy at Google Feedburner.

#### Opt Out:
You are free to avoid the RSS/ATOM feed. You may also use the source feed directly which is not tracked through Feedburner, but may be tracked in server logs per the "Other Information" section of this privacy policy.

## Other Information

Additional information may be collected by webserver logs or other logs on the server this site runs on. This information may include your IP address, the page(s) you visited, and when you visited them. This information does not contain personal details.

This information is not shared, but is used to take proactive measures to ensure the security and stability of the website, and may be used to gather aggregate metrics about site traffic.

#### Opt Out:
You cannot opt out.
